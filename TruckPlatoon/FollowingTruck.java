import java.awt.Point;
import java.io.InputStream;
import java.util.ArrayList;

public class FollowingTruck extends Truck
{
	private double X;
	private double Y;
	private double speed;
	private double acceleration;
	private boolean brake;
	private double turnAngle;
	private double gap;	
	private boolean commStatus;
	private int position;
	public final double MIN_GAP_IN_BETWEEN_TRUCKS_IN_PLATOON = 6.0;
	private ArrayList<Double> SensorData = new ArrayList<Double>();
	private ArrayList<Point2> GPSData = new ArrayList<Point2>();
	private static int counter = 0;
		
	public FollowingTruck()
	{
		super();
		this.gap = 0.0;	
		this.commStatus = true;
		this.position = 0;
		GPSData = super.readGPSData();
	}
	
	public String runTimeData()
	{
		if(counter>=GPSData.size())
		{
			counter = 0;
			GPSData = super.readGPSData();
		}
		this.setXCoord(GPSData.get(counter).x + 10);
		this.setYCoord(GPSData.get(counter).y + 10);
		counter++;
		String msg = "";
		ArrayList<String> currentValues = new ArrayList<String>();
		currentValues.add(0, "X: "+ getXcoord());
		currentValues.add(1, "Y: "+ getYcoord());
		currentValues.add(2, "speed: "+ getSpeed());
		currentValues.add(3, "acceleration: "+ getAcceleration());
		currentValues.add(4, "brake: "+ getBrakeStatus());
		currentValues.add(5, "turnAngle: "+ getTurnAngle());
		currentValues.add(6, "gap: "+ getGap());
		//System.out.println(currentValues.toString());
		return currentValues.toString();
	}
	
	public double getPosition()
	{
		return position;
	}
	
	public void setPosition(int Value) 
	{
		position = Value;	
	}
	
	
	public double getGap()
	{
		return gap;
	}
	
	public void setGap(int position) 
	{		
		gap = LENGTH_OF_TRUCK*position + MIN_GAP_IN_BETWEEN_TRUCKS_IN_PLATOON;	
	}
	
	public boolean getCommStatus() 
	{
		return commStatus;	
	}
	
	public void setCommStatus(boolean Value) 
	{
		commStatus = Value;	
	}
	
	
	public void move(double distance, double direction) 
	{
		double Ycoord = getYcoord();
		while(Ycoord < distance)
		{
			Ycoord += 1.0;
		}
		setYCoord(Ycoord);
		
	}
  
}
