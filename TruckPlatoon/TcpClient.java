//package TruckPlatoon;

import java.io.IOException;
/**
 * @name TcpClient: This class handles the function to initialize communication at client socket with specific IP and Port number
 * 
 * @version 02/25/2021
 */

import java.net.Socket;

public class TcpClient 
{
	private static final String SERVER_IP = "127.0.0.1";
	private static final int SERVER_PORT = 9090;
	
	public static void main(String[] args) throws IOException
	{
		/*Client socket*/
		Socket FTruck = new Socket(SERVER_IP, SERVER_PORT);
		
		/*Invokes the server (lead truck)  to get connected on the the specified IP and port */
		LeadTruckConnection LTserver = new LeadTruckConnection(FTruck);
		
        new Thread(LTserver).start();	

	}
		
}
