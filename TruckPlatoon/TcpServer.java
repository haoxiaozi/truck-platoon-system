//package TruckPlatoon;

import mpi.MPI;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @name TcpServer: This class handles the main function to initialize communication at server socket
 * 
 * @version 02/25/2021
 */

public class TcpServer 
{
  private static final int PORT = 9090;                                 /*Specific port no to establish client server communication*/
  private static final int MAX_NO_OF_FOLLOWING_TRUCKS = 3;              /*Total number of driver less truck in platoon */
  private static ArrayList<FollowingTruckManager> ftrucks = new ArrayList<FollowingTruckManager>();  /*Store Following Truck data in ArrayList */
  private static ExecutorService pool = Executors.newFixedThreadPool(MAX_NO_OF_FOLLOWING_TRUCKS);  /*Create thread pool for each Following Truck */
  private static int count = 0;
  
  public static void main (String[] args) throws IOException
  {
	  /*Initialize server socket with specified port no*/
	  ServerSocket leadTruck = new ServerSocket(PORT);
		
	  while(true) 
	  {
		  System.out.println("[LEAD TRUCK] Waiting for following truck's input....");
		  
		  /*Blocking call , server waits for the message from the client*/
		  Socket FTruck = leadTruck.accept();
		  count++;
		  System.out.printf("[LEAD TRUCK] Connected to following truck %1d !\n", count);
		  FollowingTruckManager ftruckThread = new FollowingTruckManager(FTruck, ftrucks);
		  
		  /*Add the following truck to the ArrayList*/
		  ftrucks.add(ftruckThread);	
		  
		  /*Execute the threads*/
		  pool.execute(ftruckThread);		  
	  }
  }  
}




