import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

/**
 * @name FollowingTruckManager : This class handles the messages received from different 
 * clients/following trucks and sends reply. This class implements runnable interface and
 * implements the run() method which is executed by the main thread of TcpServer in an
 * infinite loop for all the clients/following trucks. 
 */

public class FollowingTruckManager implements Runnable
{
	private Socket followingtruck; /*socket to recieve message from client/following truck*/
	private LeadingTruck TruckLead; /*object of Lead truck to store data of all following trucks and calulate current and expected behavior*/
	private BufferedReader in; /*Buffer of default size used to read the message received from client and put in buffer for further processing*/
	private PrintWriter out; /*To send messages to clients.*/
	private ArrayList<FollowingTruckManager> ftrucks; /*Array List to store threads executed for each following truck for sending responses to trucks simultaneously*/
	private final int MAX_NO_OF_FOLLOWING_TRUCKS = 3; /*To limit the maximum number of clients in current platoon*/
	private final int FOLLOWING_TRUCK_1 = 0; /*index of client 1 in platoon*/
	private final int FOLLOWING_TRUCK_2 = 1; /*index of client 1 in platoon*/
	private final int FOLLOWING_TRUCK_3 = 2; /*index of client 1 in platoon*/
	private final int MAX_NUM_OF_RETRIES_COMM_FAILURE = 50; /* Counter to check communication status from client side. Increment if no message recived from a particular client*/
	private final double MIN_GAP_IN_BETWEEN_TRUCKS_IN_PLATOON = 6.0; /*Ideal gap between 2 trucks*/
	private final double EPSILON = 1.0E12; /*used to compare double values of speed, acceleration and so on*/
	private int[] commCounter; /*array of counters to record number of times of messages are not received from a client to check communication failure*/
	private ArrayList<String> obstacledata; /* arraylist to store data of obstacles if any...processed in GPU by processing images catured by camera in lead truck*/
	private static int redlightcounter = 0; /*counter for red traffic light status*/
	private boolean trafficlight = false; /*flag for traffic light status green/red*/
	private ArrayList<Point2> GPSData = new ArrayList<Point2>();/* arraylist to store values of GPS coordinates received ...processed in CPU by MpiComputation*/
	
	/**
	 * @name FollowingTruckManager : Constructor for this class
	 * @param: Socket : Socket for receiving messages from following truck
	 * @param: ArrayList<FollowingTruckManager>: Array list for threads of all clients/following trucks.
	 * 
	 * Note: IOException is triggered if connection between client and server is interrupted
	 */
	public FollowingTruckManager(Socket FtruckSocket, ArrayList<FollowingTruckManager> ftrucks) throws IOException
	{
		this.followingtruck = FtruckSocket;
		this.TruckLead = new LeadingTruck();
		this.ftrucks = ftrucks;
		in = new BufferedReader(new InputStreamReader(FtruckSocket.getInputStream()));
	    out = new PrintWriter(FtruckSocket.getOutputStream(), true);
	    this.commCounter = new int[MAX_NO_OF_FOLLOWING_TRUCKS];
	    obstacledata = new ArrayList<String>();
	}
	
	/**
	 * @name run : Implements the run method of runnable interface
	 * Tcp server executed this method in a thread pool for 3 different clients
	 */
	public synchronized void run() 
    { 
		/*start the lead truck with certain speed, acceleration in front direction*/
		TruckLead.setSpeed(10.0);
		TruckLead.setAcceleration(10.0);
		TruckLead.setTurnAngle(1.0);
		obstacledata = TruckLead.readObstacleDetection(); /*read obstacle data*/
		GPSData = TruckLead.readGPSData(); /*read GPS data*/
		
	 try
	 {
		 String prev_request = "";
		 int truckNo = 0;
		 int truckIndex = 0;
		 String trucknum = "";
		 String truckname = "";
		 String request = "";
		 setPlatoonSize(); /*add 3 following trucks to platoon */
		 
		 while(true)
		 {			
		 	 request = in.readLine(); /*read message received from client*/
			 truckname = Thread.currentThread().getName(); /*check thread name and number in pool to recognise the truck sending messages*/
			 trucknum = truckname.substring(truckname.length()-1,truckname.length());
			 truckNo = Integer.parseInt(trucknum);
			 truckIndex = truckNo-1; /*Match: Array list indexes = 0, 1,2 and thread pool indexes = 1, 2, 3 */
			 TruckLead.setXCoord(GPSData.get(Controller.getGPSCounter()).x);
			 TruckLead.setYCoord(GPSData.get(Controller.getGPSCounter()).y);		 
			 if(trafficlight == true)
			 { 
				        if(redlightcounter < 10)
				        {
					    	 Broadcast("Traffic Light is Red, Wait!");
					    	 redlightcounter++;
					    	 continue;
						}
				        else {
				    	 redlightcounter = 0;
				    	 Broadcast("Traffic Light is Green, Start!");
				    	 trafficlight = false;
				        }

			 }
			 else
			 {
				 TruckLead.setBrakeStatus(false);
			 }
					 
			 if(request != null)
			 {
				 System.out.println("[FOLLOWING TRUCK" + truckNo + "] sent: " + request);
				 if(request.startsWith("UPDATE")) 
				 {
					 switch(truckIndex)
					 {			 
					  case FOLLOWING_TRUCK_1:
					  			
						  FollowingTruck truck1 = TruckLead.getFolTruck(FOLLOWING_TRUCK_1);
						  /*if current data received is same as previous data then do not update the values*/
						  if(!(prev_request.equals(request)))  assignValues(truck1, request);
						  
						  TruckLead.setFolTruck(FOLLOWING_TRUCK_1,truck1); 
						  							  
						  commCounter[FOLLOWING_TRUCK_1] = 0; /*reset comm failure counter here on re gaining communication*/
						  if(compareBehavior(TruckLead, truck1) && isGapOk(FOLLOWING_TRUCK_1))
						  {
							  sendTotruck("ACK to truck 1" , FOLLOWING_TRUCK_1) ;
						  }
						  else
						  {
							  /*Actual gap value is not proper, send Lead truck data to following truck to move accordingly*/
							  sendTotruck("UPDATE to truck 1. "+ TruckLead.runTimeData(), FOLLOWING_TRUCK_1) ;
						  }
						  
						  		if(Controller.getGPSCounter() < obstacledata.size()) /*ADD CUDA implementation for obstacle detection*/
						  		{
								 if(obstacledata.get(Controller.getGPSCounter()).contains("Pedestrian"))
								 {
									 Broadcast("ALERT: Pedestrians ahead, Lower Speed, Apply brake gradually");
									 TruckLead.setBrakeStatus(true);
								 }
								 
								 else if(obstacledata.get(Controller.getGPSCounter()).contains("Vehicle"))
								 {
									 Broadcast("ALERT: Vehicle ahead, Lower Speed, Apply brake gradually");
									 TruckLead.setBrakeStatus(true);
								 }
								 else if(obstacledata.get(Controller.getGPSCounter()).contains("Traffic"))
								 {
									 Broadcast("Traffic Light is Red, Apply Brakes");
									 trafficlight = true;
									 TruckLead.setBrakeStatus(true);
									 
								 }
								 else if(obstacledata.get(Controller.getGPSCounter()).contains("Right"))
								 {
									 Broadcast("Take Right 45 deg");
								 }
								 else if(obstacledata.get(Controller.getGPSCounter()).contains("Left"))
								 {
									 Broadcast("Take Left 45 deg");
								 }
							 
						  		}
							 
						  		else if(Controller.getGPSCounter()>=obstacledata.size())
						  		{
						  			Controller.setCounter(0);
						  		}
						  		
						  		break;
						  
					  case FOLLOWING_TRUCK_2:
						  FollowingTruck truck2 = TruckLead.getFolTruck(FOLLOWING_TRUCK_2);
						  /*if current data received is same as previous data then do not update the values*/
						  if(!(prev_request.equals(request)))  assignValues(truck2, request);
						  
						  TruckLead.setFolTruck(FOLLOWING_TRUCK_2,truck2);
						  commCounter[FOLLOWING_TRUCK_2] = 0; 
						  if(compareBehavior(TruckLead, truck2) && isGapOk(FOLLOWING_TRUCK_2))
						  {
							  sendTotruck("ACK to truck 2" , FOLLOWING_TRUCK_2) ;
						  }
						  else
						  {
							  /*Actual gap value is not proper, send Lead truck data to following truck to move accordingly*/
							  sendTotruck("UPDATE to truck 2. "+ TruckLead.runTimeData(), FOLLOWING_TRUCK_2) ;
						  }
						  if(Controller.getGPSCounter() < obstacledata.size()) /*ADD CUDA implementation for obstacle detection*/
					  		{							 
							 if(obstacledata.get(Controller.getGPSCounter()).contains("Pedestrian"))
							 {
								 Broadcast("ALERT: Pedestrians ahead, Lower Speed, Apply brake gradually");
								 TruckLead.setBrakeStatus(true);
							 }
							 
							 else if(obstacledata.get(Controller.getGPSCounter()).contains("Vehicle"))
							 {
								 Broadcast("ALERT: Vehicle ahead, Lower Speed, Apply brake gradually");
								 TruckLead.setBrakeStatus(true);
							 }
							 else if(obstacledata.get(Controller.getGPSCounter()).contains("Traffic"))
							 {
								 Broadcast("Traffic Light is Red, Apply Brakes");
								 trafficlight = true;
								 TruckLead.setBrakeStatus(true);
								 
							 }
							 else if(obstacledata.get(Controller.getGPSCounter()).contains("Right"))
							 {
								 Broadcast("Take Right 45 deg");
							 }
							 else if(obstacledata.get(Controller.getGPSCounter()).contains("Left"))
							 {
								 Broadcast("Take Left 45 deg");
							 }
						 
					  		}
						 
					  		else if(Controller.getGPSCounter()>=obstacledata.size())
					  		{
							 Controller.setGPSCounter(0);
					  		}
						  Controller.incGPSCounter();
						  break;
						  
					  case FOLLOWING_TRUCK_3:
						  FollowingTruck truck3 = TruckLead.getFolTruck(FOLLOWING_TRUCK_3);
						  /*if current data received is same as previous data then do not update the values*/
						  if(!(prev_request.equals(request)))  assignValues(truck3, request);
						  
						  TruckLead.setFolTruck(FOLLOWING_TRUCK_3,truck3);
						  commCounter[FOLLOWING_TRUCK_3] = 0;
						  if(compareBehavior(TruckLead, truck3) && isGapOk(FOLLOWING_TRUCK_3))
						  {
							  sendTotruck("ACK to truck 3" ,FOLLOWING_TRUCK_3) ;
						  }
						  else
						  {
							  /*Actual gap value is not proper, send Lead truck data to following truck to move accordingly*/
							  sendTotruck("UPDATE to truck 3. "+ TruckLead.runTimeData(), FOLLOWING_TRUCK_3) ;
						  }
						  
						  if(Controller.getGPSCounter() < obstacledata.size()) /*ADD CUDA implementation for obstacle detection*/
					  		{
							 if(obstacledata.get(Controller.getGPSCounter()).contains("Pedestrian"))
							 {								 
								 Broadcast("ALERT: Pedestrians ahead, Lower Speed, Apply brake gradually");
								 TruckLead.setBrakeStatus(true);
							 }
							 
							 else if(obstacledata.get(Controller.getGPSCounter()).contains("Vehicle"))
							 {
								 Broadcast("ALERT: Vehicle ahead, Lower Speed, Apply brake gradually");
								 TruckLead.setBrakeStatus(true);
							 }
							 else if(obstacledata.get(Controller.getGPSCounter()).contains("Traffic"))
							 {
								 sendTotruck("Traffic Light is Red, Apply Brakes Counter: "+Controller.getGPSCounter(), FOLLOWING_TRUCK_3);
								 Broadcast("Traffic Light is Red, Apply Brakes");
								 trafficlight = true;
								 TruckLead.setBrakeStatus(true);
								 
							 }
							 else if(obstacledata.get(Controller.getGPSCounter()).contains("Right"))
							 {
								 Broadcast("Take Right 45 deg");
							 }
							 else if(obstacledata.get(Controller.getGPSCounter()).contains("Left"))
							 {
								 Broadcast("Take Left 45 deg");
							 }
						 
					  		}
						 
					  		else if(Controller.getGPSCounter()>=obstacledata.size())
					  		{
							 Controller.setGPSCounter(0);
					  		}
						  
						  break;
												  
					default:
							  System.out.println("ERROR: Invalid Data Received from Client");
							  break;				  			   
					 }
				 prev_request = request;

				 }
				 else if(request.startsWith("ACK"))
				 {	
					 /*Acknowledgement of previous data sent. Send no response for ack*/
				 }
				 else
				 {
					 System.out.println("Following Truck not recognized");
					 if(commCounter[truckIndex] < MAX_NUM_OF_RETRIES_COMM_FAILURE)
					 {
						 /*if no msg received from a following truck or same message is received again increment the counter to log communication failure*/
						 commCounter[truckIndex]++;
					 }
					 
					 else 
					 {
						 /*max counter value reached, communication with Truck truckNo client failed take action*/
						 (TruckLead.getFolTruck(truckIndex)).setCommStatus(false);
						 Broadcast("ERROR: Truck " + truckNo + " lost communication");
						 isCompleteFailure(); /*Check if communication from all following trucks is failed. If yes decouple the platoon.*/					 
					 }
				 }
				 
			 }
			 /*Change the speed, acceleration of truck to check further behavior*/
			 TruckLead.setSpeed(15.0);
			 TruckLead.setAcceleration(5.0);
			 TruckLead.setTurnAngle(1.0);
			}
	 }
	 catch(IOException e)
	 {
		 System.err.println("IO exception in FollowingTruckManager");
		 System.err.println(e.getStackTrace());
	 } finally {
		 out.close();
		 try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	 }
		
	}

	/**
	 * @name Broadcast : Broadcast a msg to all clients based on currect received message
	 * @param: String : currect received message
	 */
	private void Broadcast(String msg) 
	{		
		for(int i =0; i<ftrucks.size();i++)
		{
			int temp = i+1;
			ftrucks.get(i).out.println(msg + "truck "+temp);
		}		
	}
	
	/**
	 * @name sendTotruck : Send a msg to a client based on currect received message and client index
	 * @param: String : currect received message
	 * @param: truck/client index
	 */
	private void sendTotruck(String msg, int index) 
	{
		FollowingTruckManager atruck = ftrucks.get(index);
		if(index < ftrucks.size())
		{
			atruck.out.println(msg);
		}		
	}
	
	/**
	 * @name extractValue : Extract the value of given parameter in the received message from client
	 * @param: String : currect received message
	 * @param: valueToExtract: value of parameter like speed, acceleration and so on to be extracted from received message
	 * @return: double: value of parameter
	 */
	public double extractValue(String msg, String valueToExtract)
	{
		double val = 0.0;
		if(msg.contains(valueToExtract))
		{
			int index = msg.indexOf(valueToExtract)+valueToExtract.length()+2;
			String value = msg.substring(index,index+3);
			val = Double.parseDouble(value);
		}
		return val;		
	}
	
	/**
	 * @name isBrakeApplied : Extract the value of given brakes statuso ftruck  in the received message from client
	 * @param: String : currect received message
	 * @return: boolean: true if brakes are applied
	 */
	public boolean isBrakeApplied(String msg)
	{
		boolean val = false;
		if(msg.contains("brake"))
		{
			int index = msg.indexOf("brake")+7;
			String value = msg.substring(index,index+4);
			if(value.equals("true"))
				val = true;
		}
		return val;		
	}
	
	/**
	 * @name assignValues : Assign values of speed, acceleration etc received from client truck to the local array
	 * list of lead truck to store data of following truck.
	 * @param: String : currect received message
	 * @param: FollowingTruck: following truck object in lead trucks Array list of platoon 
	 */
	public void assignValues(FollowingTruck atruck, String msg)
	{
		atruck.setXCoord(extractValue(msg, "X"));
		atruck.setYCoord(extractValue(msg, "Y"));
		atruck.setTurnAngle(extractValue(msg, "turnAngle"));
		atruck.setSpeed(extractValue(msg, "speed"));
		atruck.setAcceleration(extractValue(msg, "acceleration"));
		atruck.setBrakeStatus(isBrakeApplied(msg));
		atruck.setCommStatus(true);
	}
	
	/**
	 * @name compareBehavior : compare behavior of lead truck and following truck
	 * @param: LeadingTruck : Lead truck object
	 * @param: FollowingTruck: following truck object 
	 * @return: boolean: true if behavior of lead and following trucks is same
	 */
	public boolean compareBehavior(LeadingTruck truck1, FollowingTruck truck2)
	{
		boolean isBehaviorSame = false;
		if( (isDifferenceOk(truck1.getSpeed(),truck2.getSpeed())) || (isDifferenceOk(truck1.getAcceleration(),truck2.getAcceleration())) 
				|| (isDifferenceOk(truck1.getTurnAngle(),truck2.getTurnAngle()) || (truck1.getBrakeStatus() == truck2.getBrakeStatus()) ))
		{
			isBehaviorSame = true;
		}
		return isBehaviorSame;
	}
	
	/**
	 * @name isGapOk : Checks if gap of following truck from lead truck is ok
	 * @param: truckNumber : index of following truck
	 * @return: boolean: true if gap b/w lead and following trucks is ok
	 */
	public boolean isGapOk(int truckNumber)
	{
		boolean isOk = false;
		if(TruckLead.calculateGap(truckNumber) >= MIN_GAP_IN_BETWEEN_TRUCKS_IN_PLATOON)
		{
			isOk = true;
		}
		return isOk;
	}
	
	/**
	 * @name isDifferenceOk : Checks difference b/w 2 double values
	 * @param: double: value a
	 * @param: double value b
	 * @return: boolean: true if difference is ok
	 */
	public boolean isDifferenceOk(double a, double b)
	{
		boolean ret = false;
		if(Math.abs(a-b)<EPSILON) 
		{
			ret =  true;
		}
		return ret;
	}
	
	/**
	 * @name isCompleteFailure : Checks if server stops receiving messages from all the clients and communication is completely failed
	 */
	public void isCompleteFailure()
	{
		int completeFailure = 0;
		for(int i = 0; i< MAX_NO_OF_FOLLOWING_TRUCKS; i++)
		{
			if(!(TruckLead.getFolTruck(i).getCommStatus()))
			{
				completeFailure++;
			}
		}
		if (MAX_NO_OF_FOLLOWING_TRUCKS == completeFailure)
		{
			System.out.println("DECOUPLE THE PLATOON");		
		}
	}
	
	/**
	 * @name setPlatoonSize : adds trucks to the platoon
	 */
	public void setPlatoonSize()
	{
		FollowingTruck truck = new FollowingTruck();
		for (int i =0; i<MAX_NO_OF_FOLLOWING_TRUCKS; i++)
		{
			TruckLead.addFolTruck(i, truck);
		}
	}
	
}
	

